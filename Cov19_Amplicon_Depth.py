"""
Cov19_Amplicon
    Code to generate a bed with the region of the amplicons from the
    bed file containing the primer coordinate
Anne-Marie Roy
Summer 2020
"""

import re, os, csv, statistics, sys
import pybedtools as pbt
import pandas as pd
from os import listdir, path
from pathlib import Path

###VARIABLE
# BedPath = "../../Desktop/Raccourcis_Anne/analyse_covseq/nCoV-2019.bed"
BedPath = "./nCoV-2019.bed"

"""
The script needs 2 arguments in entry
1: Input directory -> /home/aroy4/scratch/CovSeq/AmpTemp/ (default)
2: Output directory -> /home/aroy4/scratch/CovSeq/AmpMetrics/ (default)
"""


#find the min and max of each primer pair
def Average(lst):
    if len(lst) != 0:
        return sum(lst) / len(lst)
    else:
        return 0

def find_files(dir, motif):
    files = {}
    for f in listdir(dir):
        if os.path.getsize(dir+f) != 0 and motif in f: #remove empty files and select only the bam files of the directory
            files[str(f).split(".")[0]] = str(dir)+str(f)
    return files

def find_primer_pair(file):
    region = {} #dict of amplicon region--> Name:[min(start), max(end)]
    primer_names = []
    # with open(file, "r") as f:
    f= pbt.BedTool(file)
    for line in f:
        p = re.compile("nCoV-2019_\d+(?=_)")
        primer_name = p.search(str(line).split()[3]).group()
        primer_names.append(primer_name)
    primer_names = list(dict.fromkeys(primer_names))
    #print(primer_names)
    for i in range(0, len(primer_names)):
        array = []
        for line in f:
            if primer_names[i] in str(line).split()[3]:
                array += str(line).split()[1:3]

        for j in range(0, len(array)):
            array[j] = int(array[j])
        # print(array)
        region[primer_names[i]]=[min(array),max(array)]
    #print(region)
    return region

def make_bed(regions):
    with open(outputdir+"Cov19_Amplicon.bed", "w") as outfile:
        writer = csv.writer(outfile, delimiter='\t')
        for primer, values in regions.items():
            writer.writerow(["MN908947.3", str(values[0]), str(values[1]), primer])

def meanDepth(regions, DepthFile): #DepthFile is the bam file with the calculated depth of each positions
    depths = {}
    averages = {}
    somme2 = 0
    for key, value in regions.items():
        depths[key] = []
        with open(DepthFile, "r") as f:
            for line in f:
                if (int(line.split()[1]) >= value[0]) and (int(line.split()[1]) <= value[1]):
                    depths[key] += [int(line.split()[2])]
    for key2, value2 in depths.items():
        averages[key2] = Average(value2)
        somme2 += sum(value2)
    return averages, depths

def Abundance(DepthPerRegion):
    abndt1 = {}
    NrmAvg = {}
    TotalBase = 0 #total amount of base in the sample
    for key, value in DepthPerRegion.items():
        TotalBase = TotalBase + sum(value)
    if TotalBase != 0:
        for key, value in DepthPerRegion.items():
            abndt1[key] = (sum(value)/TotalBase)
            NrmAvg[key] = (Average(value)/TotalBase)
    else:
        print(file)
    return abndt1, NrmAvg, TotalBase

def get_pctN(file_dict):
    pctN = {}
    for value in file_dict.values(): #key = name; value = path
        with open(value, "r") as f:
             reader = csv.reader(f, delimiter = ",")
             list_rows = list(reader)
             for row in list_rows:
                 if row[0] != "sample":
                     try:
                         pctN[row[0]] = round(float(row[1]),4)
                     except ValueError:
                         pctN[row[0]] = 0
    return pctN

def MinCov(DepthPerRegion, Number_pb): #DepthPerRegion is a dicto of regionName: position depth (in order)
    mincov = {}
    for key, value in DepthPerRegion.items():
        minimum = Average(value)
        for i in range(len(value)-Number_pb):
            if Average(value[i:i+Number_pb]) <= minimum:
                minimum = Average(value[i:i+Number_pb])
        mincov[key] = minimum
    return mincov

def FeatureCount(DepthPerRegion):
    FC = {}
    avg = {}
    try:
        maxV = list(DepthPerRegion.values())[0][0]
        minV = list(DepthPerRegion.values())[0][0]
    except IndexError:
        print("Failed, depth empty")
        maxV = 0
        minV = 100
    for key, value in DepthPerRegion.items():
        if value != []:
            if maxV <= max(value):
                maxV = max(value)
            if minV >= min(value):
                minV = min(value)
    for key, value in DepthPerRegion.items():
        listV = []
        if maxV-minV != 0:
            for v in value:
                v2 = (v-minV)/(maxV-minV)
                listV += [v2]
            FC[key] = listV
            avg[key] = Average(listV)
        else:
            print(key, minV, maxV)
            FC[key] = None
    return avg, FC

def writeDictToCSV(dict, path):
    df = pd.DataFrame(dict).T
    df.to_csv(path)


##Execution
inputdir = sys.argv[1] if len(sys.argv) >= 2 else '/home/aroy4/scratch/CovSeq/AmpTemp/'
outputdir = sys.argv[2] if len(sys.argv) >= 3 else '/home/aroy4/scratch/CovSeq/AmpMetrics/'
Path(outputdir).mkdir(parents=True, exist_ok=True)
region = find_primer_pair(BedPath)
make_bed(region)
print("Bed made")

metric = find_files(inputdir, ".metrics.csv")
print("metric files loaded")
pctN = get_pctN(metric)
print("%N dict made; Lenght: ", len(pctN.keys()))
files = find_files(inputdir, ".bam.depth")
print("Depth files loaded; Lenght: ", len(files.keys()))


statistics = {}
total_reads = {}
empty = []
rejected = {}

for name,value in files.items():
    if path.isfile(outputdir+"/Depth"+name+"statistics.csv"):
        print(name, " exist")
    else:
        print(name, " Started")
        avg, dpth = meanDepth(region, value)
        abndt_totalreads, NormAvg, Total = Abundance(dpth)
        try:
            pct_n = pctN[name]
        except KeyError:
            pct_n = 0
        print("Total bases: ", Total, ";  %N", pct_n)
        if Total < 30000 or pct_n > 50:
            print(name, "rejected")
            rejected[name] = [Total, pct_n]
        else:
            avgFC, NormDpth = FeatureCount(dpth)
            min1 = MinCov(dpth, 1)
            min50 = MinCov(dpth, 50)
            min100 = MinCov(dpth, 100)
            NormMin50 = MinCov(NormDpth, 50)
            NormMin25 = MinCov(NormDpth, 25)
            NormMin1 = MinCov(NormDpth, 1)
            for key in region.keys():
                try:
                    statistics[key] = {"avgDepth":avg[key], #Average coverage over all postion per amplicon
                    # "abundance1":abndt_totalreads[key], # Sum of all position coverage per amplicon divided by total position coverage in sample
                    # 'NormalizedAvgDepth':NormAvg[key], # Average coverage over all position per amplicon divided by total position coverage in sample
                    "FeatureCount":avgFC[key], #Average coverage of feature count normalization for each position
                    "MinCov_1pb":min1[key], #Minimun coverage observed over 1 position
                    "MinCov_50pb":min50[key], #Minimun coverage observed over 50 consecutive positions
                    "MinCov_100pb":min100[key], #Minimun coverage observed over 100 consecutive positions
                    "NormMinCov_1pb":NormMin1[key], #Minimun coverage observed over 1 position
                    "NormMinCov_25pb":NormMin25[key], #Minimun coverage observed over 50 consecutive positions
                    "NormMinCov_50pb":NormMin50[key]} #Minimun coverage observed over 100 consecutive positions
                except KeyError:
                    empty += [name, key]
                    print(key, "is empty")
            writeDictToCSV(statistics, outputdir+"/Depth"+name+"statistics.csv")
            total_reads[name] = Total
        print(name, " _Completed, Total base reads: ", Total)

#Save Total reads per sample in a CSV
df = pd.DataFrame(total_reads, index = ["TotalCovPerPosition"]).T
if path.isfile(outputdir+"/Total_reads_stats.csv"):
    df1 = pd.read_csv(outputdir+"/Total_reads_stats.csv")
    df = df1.append(df)
df.to_csv(outputdir+"/Total_reads_stats.csv")
#Save rejected Samples to a CSV
writeDictToCSV(rejected, outputdir+"Rejected_Samples.csv")

#Save failedSample to a csv
with open(outputdir+'list_empty_sample.csv', 'w', newline='') as myfile:
     wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
     wr.writerow(empty)
