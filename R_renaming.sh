cd /home/aroy4/scratch/CovSeq/AmpTemp/

for file in *;
do
  if [[ $file =~ ^[0-9] ]]; then
    mv $file "X$file"
  fi
done

for file in *-*; do
  mv $file "${file//-/}"
done
