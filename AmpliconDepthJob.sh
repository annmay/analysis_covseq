#!/bin/bash
#SBATCH --account=def-ioannisr
#SBATCH --time=10:00:00
#SBATCH --job-name=AmpliconDepth
#SBATCH --output=/home/aroy4/scratch/CovSeq/NanoDepth.out

now=$(date '+%Y%m%d')

echo `date`
module load python/3.8.2
source amp-env/bin/activate
module load samtools/1.10

bash /home/aroy4/scratch/gather_nanopore_depth_file.sh
bash /home/aroy4/scratch/gather_illumina_depth_file.sh
bash /home/aroy4/scratch/source_sample.sh

bash /home/aroy4/scratch/R_renaming.sh

ls -1 /home/aroy4/scratch/CovSeq/AmpTemp/ | grep depth | sed -e 's/\.sorted.*bam.depth$//' > /home/aroy4/scratch/CovSeq/Samples_$now.txt
find /home/aroy4/scratch/CovSeq/AmpTemp/ -size 0 > /home/aroy4/scratch/CovSeq/Empty_files_$now.txt

python Cov19_Amplicon_Depth.py


echo "Samples (AmpTemp)"
wc -l /home/aroy4/scratch/CovSeq/Samples_$now.txt
echo "\n Samples (AmpMetrics)"
find /home/aroy4/scratch/CovSeq/AmpMetrics/Depth* | wc -l
echo "\n \n process finished"
