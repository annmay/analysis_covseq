#!/bin/bash
now=$(date '+%Y%m%d')

for f in `find /genfs/projects/COVID_full_processing/ -name '*filtered.bam'`
do
   filename=`echo $f|awk -F'/' '{SL = NF-1; TL = NF-3; PL = NF-4; print $PL "," $TL "," $SL }'`
   echo $filename >> ~/scratch/source_$now.txt
done

for f in `find /genfs/projects/COVID_full_processing/ -name '*sorted.bam.depth'`
do
   filename=`echo $f|awk -F'/' '{SL = NF-1; TL = NF-4; print "," $TL "," $SL}'`
   echo $filename >> ~/scratch/source_$now.txt
done
