#All sample from Beluga; to be updated as we sequence more samples 
#20200910: 3226 samples

Samples <- read.csv(paste(ProjectPath,"AmpAnalysis/Sample_list.csv", sep=""), skip = 1)
# Samples <- filter(Samples, Sequencer %in% c("illumina", "nanopore"))
Samples <- filter(Samples, smpl %in% unique(drop_na(list_df[[2]])$smpl))


##Colname = "Sequencer" "RunName"   "smpl"  "RT_enz"  "PCR_temp"  "Primer" "LA"  "Library"

set.seed(42)
#iC = index de la condition; C = condition (C1 and C2); K = constante -> must be unique in the group

GroupUnpaired <- function(ref, iC1, C1, iC2, C2, colName){
  #1st filter -> C1 
  global <- filter(ref, ref[iC1] == C1)
  group <- list()
  for (i in 1:length(C2)){
    group[[i]] <- global %>% filter(global[iC2] == C2[i])
    #print(unique(group[[i]]$smpl))
    group[[i]] <- group[[i]] %>% distinct(smpl, .keep_all = TRUE)
    # print(length(group[[i]]$smpl))
    # print(unique(unlist(group[[i]][4:8])))
  }
  
  #print(sapply(group, nrow))

  min <- min(sapply(group, nrow))
  print(min)
  
  vectGroup <- list()
  for (i in 1:length(group)){
    group[[i]] <- group[[i]][sample(nrow(group[[i]]), min), ]
    # print(paste(unique(group[i]), "; len = ", length(group[[i]]$smpl)))
    vectGroup[[i]] <- c(group[[i]]$smpl)
  }
  
  compGroup <- data.frame(do.call(cbind, vectGroup))
  names(compGroup) <- colName 
  
  return(compGroup)
}

#Comparison 65wLA1 and 63w/oLA1
globaltemp <- filter(Samples, Samples[4] == "lunascript")
group65 <- globaltemp %>% filter(globaltemp[5] == "65" & globaltemp[7] == "la1")
group63 <- globaltemp %>% filter(globaltemp[5] == "63" & globaltemp[7] == "none")

group <- list(group65, group63)

min <- min(sapply(group, nrow))
print(min)

vectGroup <- list()
for (i in 1:length(group)){
  group[[i]] <- group[[i]][sample(nrow(group[[i]]), min), ]
  # print(paste(unique(group[i]), "; len = ", length(group[[i]]$smpl)))
  vectGroup[[i]] <- c(group[[i]]$smpl)
}

compGroup <- data.frame(do.call(cbind, vectGroup))
colName <- c("Temp65wla1", "Temp63nola1")
names(compGroup) <- colName 

list_df_unpaired <- list() 
#1st condition 65C; impact of LA1 
# list_df_unpaired[[1]] <- GroupUnpaired(Samples,5, "65", 7, c("none", "la1"),  c("noLA", "LA1"))
# # 2nd condition 63C; impact of LA1 
# list_df_unpaired[[2]] <- GroupUnpaired(Samples,5, "63", 7, c("none", "la1"), c("noLA", "LA1"))
# # 3rd condition w/ LA; 63 vs 65
# list_df_unpaired[[3]] <- GroupUnpaired(Samples, 7,  "la1", 5, c("63", "65"), c("Temp63", "Temp65"))
# #4thcondition no LA; 63 vs 65
# list_df_unpaired[[4]] <- GroupUnpaired(Samples, 7,  "none", 5, c("63", "65"), c("Temp63", "Temp65"))
# # 6th condition SSIV; Nanopore lib prep 
# list_df_unpaired[[6]] <- GroupUnpaired(Samples, 4, "SSIV", 8, c("NanoArticV3", "Nano10", "Nano50"), c("Nano5ul", "Nano10ul", "Nano50ul"))
# 
# list_df_unpaired[[5]] <- compGroup
# 
# #7thcondition wLA; Lunascript vs SSIV
# list_df_unpaired[[7]] <- GroupUnpaired(Samples, 7, "la1", 4, c("SSIV", "lunascript"), c("SSIV42", "Lunascript"))
# # 8th condition wla1; All RTenzyme
# list_df_unpaired[[8]] <- GroupUnpaired(Samples, 7, "la1", 4, c("SSIV", "lunascript", "maxima"), c("SSIV42", "Lunascript", "Maxima"))
# #9th condition primers Artic vs NEB 
list_df_unpaired[[1]] <- GroupUnpaired(Samples, 5, "63", 6, c("V3_only", "NEB"), c("Artic", "NEB"))


names(list_df_unpaired) <- c("Comparison of Primers efficiency - test of NEB primers mix")

# names(list_df_unpaired) <- c("Comparison of Amplicon Depth when PCR Temperature is at 65°C with and without Pool LA1", 
#                              "Comparison of Amplicon Depth when PCR Temperature is at 63°C with and without Pool LA1",
#                              "Comparison of Amplicon Depth with Pool LA1 for different PCR Temperatures", 
#                              "Comparison of Amplicon Depth without Pool LA1 for different PCR Temperatures", 
#                              "Comparison of Amplicon Depth between PCR Temperature at 65°C with Pool LA1 and Temperature at 63°C witout Pool LA1",
#                              "Comparison of Amplicon Depth with RT enzyme SSIV for different Nanopore Library Preparations", 
#                              "Comparison of Amplicon Depth with Pool LA1 for two different RT enzymes", 
#                              "Comparison of Amplicon Depth with Pool LA1 for three different RT enzymes")
# 

# # 7th condition; Illumina Library 
# illuminaLibPrep <- GroupUnpaired(Samples, 8, c("ClnPlx", "NxFlx", "Nano10"), c("ClnPlx", "NxFlx", "Nano10"))


SampleAvg <- filter(Samples, smpl %in% unique(drop_na(list_df[[1]])$smpl))
#Comparison 65wLA1 and 63w/oLA1
globaltemp <- filter(SampleAvg, SampleAvg[4] == "lunascript")
group65 <- globaltemp %>% filter(globaltemp[5] == "65" & globaltemp[7] == "la1")
group63 <- globaltemp %>% filter(globaltemp[5] == "63" & globaltemp[7] == "none")

group <- list(group65, group63)

min <- min(sapply(group, nrow))
print(min)

vectGroup <- list()
for (i in 1:length(group)){
  group[[i]] <- group[[i]][sample(nrow(group[[i]]), min), ]
  # print(paste(unique(group[i]), "; len = ", length(group[[i]]$smpl)))
  vectGroup[[i]] <- c(group[[i]]$smpl)
}

compGroup <- data.frame(do.call(cbind, vectGroup))
colName <- c("Temp65wla1", "Temp63nola1")
names(compGroup) <- colName 

list_df_unpaired_avg <- list_df_unpaired 
# #1st condition 65C; impact of LA1 
# list_df_unpaired_avg[[1]] <- GroupUnpaired(Samples,5, "65", 7, c("none", "la1"),  c("noLA", "LA1"))
# # 2nd condition 63C; impact of LA1 
# list_df_unpaired_avg[[2]] <- GroupUnpaired(Samples,5, "63", 7, c("none", "la1"), c("noLA", "LA1"))
# # 3rd condition w/ LA; 63 vs 65
# list_df_unpaired_avg[[3]] <- GroupUnpaired(Samples, 7,  "la1", 5, c("63", "65"), c("Temp63", "Temp65"))
# #4thcondition no LA; 63 vs 65
# list_df_unpaired_avg[[4]] <- GroupUnpaired(Samples, 7,  "none", 5, c("63", "65"), c("Temp63", "Temp65"))
# # 6th condition SSIV; Nanopore lib prep 
# list_df_unpaired_avg[[6]] <- GroupUnpaired(Samples, 4, "SSIV", 8, c("NanoArticV3", "Nano10", "Nano50"), c("Nano5ul", "Nano10ul", "Nano50ul"))
# 
# list_df_unpaired_avg[[5]] <- compGroup
# 
# #7thcondition wLA; Lunascript vs SSIV
# list_df_unpaired_avg[[7]] <- GroupUnpaired(Samples, 7, "la1", 4, c("SSIV", "lunascript"), c("SSIV42", "Lunascript"))
# # 8th condition wla1; All RTenzyme
# list_df_unpaired_avg[[8]] <- GroupUnpaired(Samples, 7, "la1", 4, c("SSIV", "lunascript", "maxima"), c("SSIV42", "Lunascript", "Maxima"))
# 
# 
# 
# 
# names(list_df_unpaired_avg) <- c("Comparison of Amplicon Depth when PCR Temperature is at 65°C with and without Pool LA1", 
#                              "Comparison of Amplicon Depth when PCR Temperature is at 63°C with and without Pool LA1",
#                              "Comparison of Amplicon Depth with Pool LA1 for different PCR Temperatures", 
#                              "Comparison of Amplicon Depth without Pool LA1 for different PCR Temperatures", 
#                              "Comparison of Amplicon Depth between PCR Temperature at 65°C with Pool LA1 and Temperature at 63°C witout Pool LA1",
#                              "Comparison of Amplicon Depth with RT enzyme SSIV for different Nanopore Library Preparations", 
#                              "Comparison of Amplicon Depth with Pool LA1 for two different RT enzymes", 
#                              "Comparison of Amplicon Depth with Pool LA1 for three different RT enzymes")
# 
