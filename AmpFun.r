#Functions used in the Amplicon Depth analysis 
#Author: Anne-Marie Roy 
#Date: Summer 2020 

list.of.packages <- c("plyr", "readr", "tidyverse", "RColorBrewer", "pals", "reshape2", "ggpubr")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages, repos='https://cloud.r-project.org/')
lapply(list.of.packages, library, character.only = TRUE)



##Data Formating (from csv to dataframe)
CompilerCSV <- function(dat_csv, filelist, pattern, n){
  if (is.null(dat_csv)){
    dat_csv<- read.csv(filelist[1])[1] %>% dplyr::rename("amplicon" = X)
  }
  for (file in filelist){
    nom <- gsub(pattern, "", file)
    if (!(nom %in% names(dat_csv))){
      # print(nom)
      temp <- read.csv(file)[1] %>% bind_cols(read.csv(file)[n])
      colnames(temp) <- c("amplicon", nom)
      dat_csv <- dat_csv %>% join(temp, by = "amplicon")
    }
  }
  return(dat_csv)
}

basicGraph <- function(df, Title, Subtitle){
  colourCount = length(unique(df$amplicon))
  line.data <- data.frame(yintercept = c(median(df$stat)), Lines = c("Median"))
  
  g <- ggplot(df, aes(amplicon, stat)) + 
    geom_boxplot(aes(fill=amplicon)) +
    scale_fill_manual(values = rep(as.vector(stepped(24)),5)[1:colourCount]) +
    geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1)+
    labs(title=Title, 
         subtitle = Subtitle) +
    theme(legend.position="bottom", 
          plot.title = element_text(hjust = 0.5), 
          plot.subtitle = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2)) +
    guides(fill=guide_legend(nrow=10)) 
  return(g)
}

StatsGraph <- function(df, Title, Subtitle, LA_primers){
  lineV.data <- data.frame(xintercept = LA_primer)
  heigthP <- max(df$stat)*0.02
  # print(heigthP)
  
  g <- basicGraph(df, Title, Subtitle) + 
    geom_vline(aes(xintercept = xintercept), lineV.data, lwd=0.7)+
    stat_compare_means(method = "anova", label.x = 4, label.y = 0-max(df$stat)*0.008) +
    stat_compare_means(method = "t.test", ref.group = ".all.", hide.ns = TRUE,
                       label = "p.signif", position = position_jitter(width = 0, height = heigthP, seed = 4))
    
}

StatsGraphGroup <- function(df, Title, Subtitle, LA_primers){
  line.data <- data.frame(yintercept = c(median(df$stat)), Lines = c("Median"))
  lineV.data <- data.frame(xintercept = LA_primer)
  heigthP <- max(df$stat)*0.02
  
  g <- ggplot(df, aes(amplicon, stat)) + 
    geom_boxplot(aes(fill=group)) +
    geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1)+
    geom_vline(aes(xintercept = xintercept), lineV.data, lwd=0.7)+
    labs(title=Title, 
         subtitle = Subtitle) +
    theme(legend.position="bottom", 
          plot.title = element_text(hjust = 0.5), 
          plot.subtitle = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2)) +
    guides(fill=guide_legend(nrow=10)) 
  return(g)
}

GroupPlotUnpaired <- function(df, Title, subtitle, LA_primer) {
  line.data <- data.frame(yintercept = c(median(df$stat)), Lines = c("Median"))
  lineV.data <- data.frame(xintercept = LA_primer)
  # print(str(df))
  
  g <-ggplot(df, aes(group, stat)) + 
    geom_boxplot(aes(fill=group)) +
    geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1)+
    facet_wrap(~amplicon,  scales = "free_y") +
    stat_compare_means(label = "p.format", method = "t.test", label.x = 0.7, label.y = 0) +
    # geom_rect(data=subset(df, amplicon %in% LA_primer),
    #           #inherit.aes = F,
    #           aes(xmin=-Inf, xmax=Inf, ymin=-Inf, ymax=Inf), 
    #           fill="lightseagreen", alpha=0.03)+
    labs(title=Title, 
         subtitle = subtitle) +
    theme(legend.position="left", 
          plot.title = element_text(hjust = 0.5), 
          plot.subtitle = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2))
  
  if (length(unique(df$smpl))<20){
    g <- g + geom_jitter(position=position_jitter(0.1))
  }
  
  return(g)
}

GroupPlotPaired <- function(df, Title, subtitle, LA_primer) {
  line.data <- data.frame(yintercept = c(median(df$stat)), Lines = c("Median"))
  lineV.data <- data.frame(xintercept = LA_primer)
  
  g <-ggplot(df, aes(group, stat)) + 
    geom_boxplot(aes(fill=group)) +
    geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1)+
    geom_vline(aes(xintercept = xintercept), lineV.data, lwd=0.7)+
    facet_wrap(~amplicon,  scales = "free_y") +
    stat_compare_means(label = "p.format", method = "t.test", paired = TRUE, label.x = 0.7, label.y =0) +
    # geom_rect(data=subset(df, amplicon %in% LA_primer), 
    #           aes(xmin=-Inf, xmax=Inf, ymin=-Inf, ymax=Inf), 
    #           fill="lightseagreen", alpha=0.03)+
    labs(title=Title, 
         subtitle = subtitle) +
    theme(legend.position="left", 
          plot.title = element_text(hjust = 0.5), 
          plot.subtitle = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2))
  
  if (length(unique(df$smpl))<20){
    g <- g + geom_jitter(position=position_jitter(0.1))
  }
  
  return(g)
}

PairedPlot <- function(df, Title, Subtitle){
  g <- ggpaired(df, x= "group", y= "stat", color = "group", palette = "jco", 
                line.color = "gray", line.size = 0.4,
                facet.by = "dose", short.panel.labs = FALSE) +
    facet_wrap(~amplicon,  scales = "free_y") + 
    # geom_text(data = df, aes(label = smpl)) +
    stat_compare_means(method = "t.test", paired = TRUE) +
    labs(title=Title, 
         subtitle = Subtitle) +
    theme(legend.position="left",
          plot.title = element_text(hjust = 0.5),
          plot.subtitle = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2))
  return(g)
}