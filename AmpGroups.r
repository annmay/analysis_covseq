#This script serve to determine the comparison groups to be use in the analysis 
#Author: Anne-Marie Roy 
#Date: Summer 2020

"Usage: 
Create your group in a csv of two columns and add the path to the Paired or Unpaired groups 
"
# library(tidyverse)


#Unpaired comparison groups 
Path <- paste(baseFolder, "analysis_covseq/AmpUnpairedGroup.r", sep="")

source(Path)
if (length(list_df_unpaired)>0){
  for (i in 1:length(list_df_unpaired)){
    df <- list_df_unpaired[[i]]
    df <- pivot_longer(df, cols = 1:length(df), names_to = "group", values_to = "smpl")
    list_df_unpaired[[i]] <- df
  }
}

if (length(list_df_unpaired_avg)>0){
  for (i in 1:length(list_df_unpaired_avg)){
    df <- list_df_unpaired_avg[[i]]
    df <- pivot_longer(df, cols = 1:length(df), names_to = "group", values_to = "smpl")
    list_df_unpaired_avg[[i]] <- df
  }
}


 #Paired Comparison groups 
Path <- paste(ProjectPath, "AmpAnalysis/Paired", sep="")


list_csv_paired <- list.files(path= Path, pattern = "*csv", full.names=TRUE)

list_df_paired <- list() 
if (length(list_csv_paired > 0)){
    list_df_paired  <- lapply(list_csv_paired, read_csv, skip = 1)
    names(list_df_paired) <- gsub(paste(Path, "/|\\.csv$", sep = ""), "", list_csv_paired)
}
if (length(list_df_paired)>0){
  for (i in 1:length(list_df_paired)){
    df <- list_df_paired[[i]]
    lvl = unlist(c(df[,1],df[,2]), use.names = FALSE)
    df <- pivot_longer(df, cols = 1:2, names_to = "group", values_to = "smpl")
    df <- df %>% arrange(factor(smpl, levels = lvl))
    list_df_paired[[i]] <- df
  }
  
}

