#!/bin/bash
#SBATCH --account=def-ioannisr
#SBATCH --time=10:00:00
#SBATCH --job-name=AmpliconDepth
#SBATCH --output=/home/aroy4/scratch/NanoDepth.out

now=$(date '+%Y%m%d')

echo `date`
module load python/3.8.2
source amp-env/bin/activate
module load samtools/1.10
module load pandas/1.1.3

#bash /home/aroy4/scratch/gather_nanopore_smpl_specific_depth_file.sh
#bash /home/aroy4/scratch/gather_illumina_depth_file.sh
#bash /home/aroy4/scratch/source_sample.sh

#bash ./R_renaming.sh

ls -1 ./AmpTemp/ | grep depth | sed -e 's/\.sorted.*bam.depth$//' > ./Samples_$now.txt
find ./AmpTemp/ -size 0 > ./Empty_files_$now.txt

python Cov19_Amplicon_Depth.py ./AmpTemp/ ./AmpMetrics/


echo "Samples (AmpTemp)"
wc -l /home/aroy4/scratch/Samples_$now.txt
echo "\n Samples (AmpMetrics)"
find /home/aroy4/scratch/AmpMetrics/Depth* | wc -l
echo "\n \n process finished"
