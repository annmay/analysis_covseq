#Formating the data for the Amplicon Depth analysis 
#Author: Anne-Marie Roy 
#Date: Summer 2020 

##Adapt those variable to your environement, give absolute path
FunctionFile <- paste(baseFolder, "analysis_covseq/AmpFun.r", sep="")
ProjectPath <- paste(baseFolder, "Covseq/", sep="")
GroupFunction <-  paste(baseFolder, "analysis_covseq/AmpGroups.r", sep="")


# setwd(ProjectPath)
source(FunctionFile)


FilePattern <- "Depth|_*statistics.csv"

path.files <- paste(ProjectPath, "AmpMetrics", sep="")
files <- list.files(path=path.files , pattern = FilePattern, full.names=TRUE)
ListPattern <- paste(ProjectPath, "AmpMetrics/", FilePattern, sep = "")

df_names <-names(read.csv(files[1][1]))
list_df <- list()
for (i in c(2:length(df_names))){
  name <- df_names[i]
  myfile = paste(ProjectPath, "AmpCompiledMetrics/Amp_", name, ".csv", sep="")
  if (file.exists(myfile)){
    dat_csv <- read.csv(myfile)
  }else{
    dat_csv = NULL
  }
  temp <- CompilerCSV(dat_csv, files, ListPattern, i)
  write.csv(temp, file = myfile, row.names = FALSE)
  temp <- temp %>% gather(key = "smpl", value = stat, -amplicon)
  temp$amplicon <- factor(temp$amplicon, levels = unique(temp$amplicon))
  temp <- temp %>% drop_na()
  list_df[[name]] <- temp
  # assign(name, temp)
}



source(GroupFunction)

df <- list_df[[2]] #%>% drop_na()
list_group_Unpaired <- list()

if (length(list_df_unpaired)>0){
  Unpaired_group_names <- names(list_df_unpaired)
  for (i in 1:length(list_df_unpaired)){
    if (length(list_df_unpaired[[i]]$smpl)>0){
      name <- Unpaired_group_names[i]
      subdf <- df %>% inner_join(list_df_unpaired[[i]], by = "smpl") ##2 is the FC normalisation 
      list_group_Unpaired[[name]] <- subdf
    }
  }
}

df <- list_df[[1]] 
list_group_Unpaired_avg <- list()

if (length(list_df_unpaired_avg)>0){
  Unpaired_group_names_avg <- names(list_df_unpaired_avg)
  for (i in 1:length(list_df_unpaired_avg)){
    if (length(list_df_unpaired_avg[[i]]$smpl)>0){
      name <- Unpaired_group_names_avg[i]
      subdf <- df %>% inner_join(list_df_unpaired_avg[[i]], by = "smpl") ##2 is the FC normalisation 
      list_group_Unpaired_avg[[name]] <- subdf
    }
  }
}

missing <- setdiff(list_df[[1]]$smpl, list_df_unpaired[[1]]$smpl)
write.csv(missing, file = 'missing.csv', row.names = FALSE)


list_group_Paired <- list()
if (length(list_df_paired)>0){
  Paired_group_names <- names(list_df_paired)
  for (i in 1:length(list_df_paired)){
    name <- Paired_group_names[i]
    subdf <- df %>% inner_join(list_df_paired[[i]], by = "smpl") ##2 is the FC normalisation
    lvl <- unique(list_df_paired[[i]]$smpl)
    subdf <- subdf %>% arrange(factor(smpl, levels = lvl))
    list_group_Paired[[name]] <- subdf  
  }
}

