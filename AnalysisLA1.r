# Program to analyze the impact of LA1 primer pool on amplicon coverage
# Anne-Marie Roy
# Summer 2020

#Path for the file w/ functions (AmpliconStats.r)
# pathfun = "C:/Users/pepel/Desktop/Raccourcis_Anne/analyse_covseq/AmpliconStats.r"
pathfun = "/Users/aroy/BioinformaticsWorks/analysis_covseq/AmpliconStats.r"

#Library 
source(pathfun)
library(ggpubr)

#Define path for the stats files 
# path = "/Users/aroy/BioinformaticsWorks/CovidNano_Stats"
path = "/Users/aroy/BioinformaticsWorks/ArticNano_TestStats"
# path = "C:/Users/pepel/anmay/BioInfo/DepthStat"

#Data 
#make a function to extract all data from the folder and import it. put the file name in a list 
files <- list.files(path = path)
Global <- files[length(files)]
files <- files[1:length(files)-1]
list_df <- list()

# data wrangling 
setwd(path)
stats_df <- read.csv(files[1])[1]
colnames(stats_df) <- c("amplicon")
ord <- factor(stats_df$amplicon, levels = unique(stats_df$amplicon))

stats_avgCov <- Merging_Stats(files, 2)
stats_Norm <- Merging_Stats(files, 5)
stats_FC <- Merging_Stats(files, 4)
stats_min1 <- Merging_Stats(files, 6)
stats_min50 <- Merging_Stats(files, 7)
stats_min100 <- Merging_Stats(files, 8)
NMin1 <- Merging_Stats(files, 9)
NMin25 <- Merging_Stats(files, 10)
NMin50 <- Merging_Stats(files, 11)

setwd("../")

# Group of samples 
G1_62 = c("MGC001", "MGC003", "MGC005", "MGC015")
G1_63 = c("MGC002", "MGC004", "MGC006", "MGC018")
dfG1 <- data.frame(smpl = c(G1_62, G1_63), group = c(rep("62.5", 4), rep("63",4)))

G2_0114 = c("MGC007", "MGC011", "MGC013")
G2_05 = c("MGC008", "MGC012", "MGC014")
dfG2 <- data.frame(smpl = c(G2_0114, G2_05), group = c(rep("0.114", 3), rep("0.5",3)))


G3_62 = c("MGC013", "MGC014", "MGC019", "MGC021")
G3_63 = c("MGC016", "MGC017", "MGC020", "MGC022")
dfG3 <- data.frame(smpl = c(G3_62, G3_63), group = c(rep("62.5", 4), rep("63",4)))

G4_LA1 = c("MGC014", "MGC017", "MGC022")
G4_no = c("MGC015", "MGC018", "MGC023")
dfG4 <- data.frame(smpl = c(G4_LA1, G4_no), group = c(rep("LA1", 3), rep("noLA1",3)))

noLa1 = c("MGC002","MGC004","MGC006","MGC018", "MGC015", "MGC018")




AnalysisLA1 <- function(df, GraphTitle, filename) {
  LA1 <- subset(df, smpl %in% noLa1) %>% full_join(dfG1, by = "smpl")
  G1 <- subset(df, smpl %in% c(G1_62, G1_63)) %>% full_join(dfG1, by = "smpl")
  G2 <- subset(df, smpl %in% c(G2_0114, G2_05)) %>% full_join(dfG2, by = "smpl")
  G3 <- subset(df, smpl %in% c(G3_62, G3_63)) %>% full_join(dfG3, by = "smpl")
  G4 <- subset(df, smpl %in% c(G4_LA1, G4_no)) %>% full_join(dfG4, by = "smpl")
  
  g1 <- Stats_graph(df, GraphTitle, "All samples, n=23")
  g1a <- Stats_graph_no800(LA1, GraphTitle, "All samples without LA1, n=6")
  g2 <- Stats_graph_Compare(G1, GraphTitle, "no LA1, Impact of Temperature, n=4")
  g3 <- Stats_graph_CompareFacet(G1, GraphTitle, "no LA1, Impact of Temperature, n=4")
  g3a <- Stats_graph_CompareLA1Facet(G1, GraphTitle, "no LA1, Impact of Temperature, n=4")
  g4 <- Stats_graph_Compare(G2, GraphTitle, "Impact of LA1 ratio, n=3")
  g5 <- Stats_graph_CompareFacet(G2, GraphTitle,  "Impact of LA1 ratio, n=3")
  g5a <- Stats_graph_CompareLA1Facet(G2, GraphTitle,  "Impact of LA1 ratio, n=3")
  g6 <- Stats_graph_Compare(G3, GraphTitle, "with LA1, Impact of Temperature, n=4")
  g7 <- Stats_graph_CompareFacet(G3, GraphTitle, "with LA1, Impact of Temperature, n=4")
  g7a <- Stats_graph_CompareLA1Facet(G3, GraphTitle, "with LA1, Impact of Temperature, n=4")
  g8 <- Stats_graph_Compare(G4, GraphTitle, "Pool LA1 impact, n=3")
  g9 <- Stats_graph_CompareFacet(G4, GraphTitle, "Pool LA1 impact, n=3")
  g9a <- Stats_graph_CompareLA1Facet(G4, GraphTitle, "Pool LA1 impact, n=3")
  plots <- list(g1,g1a,g2,g3,g3a,g4,g5,g5a,g6,g7,g7a,g8,g9,g9a)
  # ggsave(filename, plot = plots, width = 28, height = 20)
  pdf(filename, width = 28, height = 20)
  for (p in plots){
    print(p)
  }
  dev.off()
  invisible(NULL)
  # return(list(g1,g1a,g2,g3,g3a,g4,g5,g5a,g6,g7,g7a,g8,g9,g9a))
  
}


# graphG <- AnalysisLA1(stats_avgCov, "Average Coverage per amplicon")
# pdf("Rplot_Optimisation20200731_AvgCov.pdf", width = 28, height = 20)

AnalysisLA1(stats_avgCov, "Average Coverage per amplicon", "Rplot_Optimisation20200720_AvgCovtest.pdf")
# pdf("Rplot_Optimisation20200720_AvgCov.pdf", width = 28, height = 20)
# graphG[1]
# graphG[2]
# graphG[3]
# graphG[4]
# graphG[5]
# graphG[6]
# graphG[7]
# graphG[8]
# graphG[9]
# graphG[10]
# graphG[11]
# graphG[12]
# graphG[13]
# graphG[14]
# dev.off()

# 
# graphG <- AnalysisLA1(stats_FC, "Average Normalized Reads per amplicon")
# pdf("Rplot_Optimisation20200731_NormalizedFC.pdf", width = 28, height = 20)
# graphG[1]
# graphG[2]
# graphG[3]
# graphG[4]
# graphG[5]
# graphG[6]
# graphG[7]
# graphG[8]
# graphG[9]
# graphG[10]
# graphG[11]
# graphG[12]
# graphG[13]
# graphG[14]
# dev.off()
# 
# graphG <- AnalysisLA1(stats_Norm, "Average Normalized Coverage per amplicon")
# pdf("Rplot_Optimisation20200731_NormalizadTotal.pdf", width = 28, height = 20)
# graphG[1]
# graphG[2]
# graphG[3]
# graphG[4]
# graphG[5]
# graphG[6]
# graphG[7]
# graphG[8]
# graphG[9]
# graphG[10]
# graphG[11]
# graphG[12]
# graphG[13]
# graphG[14]
# dev.off()
# 
# g1 <- Stats_graph(stats_min1, "Minimal Coverage over 1 bp", "All samples, n=23")
# g2 <- Stats_graph(stats_min50, "Minimal Coverage over 50 bp", "All samples, n=23")
# g3 <- Stats_graph(stats_min100, "Minimal Coverage over 100 bp", "All samples, n=23")
# g4 <- Stats_graph_no800(NMin1, "Normlaized Minimal Coverage over 1 bp", "All samples, n=23")
# g5 <- Stats_graph_no800(NMin25, "Normlaized Minimal Coverage over 25 bp", "All samples, n=23")
# g6 <- Stats_graph_no800(NMin50, "Normlaized Minimal Coverage over 50 bp", "All samples, n=23")
# 
# pdf("Rplot_MinimalCoverage.pdf",  width = 28, height = 20)
# g1
# g2
# g3
# g4
# g5
# g6
# dev.off()
# 
# #means comparison 
# names(stats_avgCov)[names(stats_avgCov) == "stat"] <- "value"
# names(stats_FC)[names(stats_FC) == "stat"] <- "value"
# 
# Avg <- stats_avgCov %>% summarise_at(vars(value), list(avg = mean)) 
# AvgAmp <- stats_avgCov %>% group_by(amplicon) %>% summarise_at(vars(value), list(avg = mean)) 
# temp <- stats_FC %>% group_by(amplicon) %>% summarise_at(vars(value), list(avgNorm = mean)) 
# LA1 <- subset(stats_avgCov, smpl %in% noLa1) %>% group_by(amplicon) %>% 
#   summarise_at(vars(value), list(avg_noLA1 = mean))
# 
# AvgAmp <- bind_cols(AvgAmp, temp[2])
# AvgAmp <- bind_cols(AvgAmp, LA1[2])
# Avg <- bind_cols(Avg, summarise_(stats_FC, avgNorm = mean(stats_FC$value)))
# 
# 
# LA1 <- subset(stats_avgCov, smpl %in% noLa1)
# Avg <- bind_cols(Avg, summarise_(LA1, avg_noLA = mean(LA1$value)))
# 
# Ttest_avgCov <- compare_means(value~amplicon, stats_avgCov, method = "t.test", ref.group =".all.")
# write.csv(Ttest_avgCov, "Ttest_avgCov_20200731.csv")
# t <- compare_means(value~amplicon, LA1, method = "t.test", ref.group =".all.")
# write.csv(Ttest_avgCov, "Ttest_avgCov_noLA1_20200731.csv")
# 
# 
# LA1 <- subset(stats_FC, smpl %in% noLa1) %>% group_by(amplicon) %>% 
#   summarise_at(vars(value), list(avgNorm_noLA1 = mean))
# 
# AvgAmp <- bind_cols(AvgAmp[1:4], LA1[2])
# LA1 <- subset(stats_FC, smpl %in% noLa1)
# Avg <- bind_cols(Avg, summarise_(LA1, avgNorm_noLA = mean(LA1$value)))
# Ttest_FC <- compare_means(value~amplicon, stats_FC, method = "t.test", ref.group =".all.")
# write.csv(Ttest_FC, "Ttest_FeatureNorm_20200731.csv")
# t <- compare_means(value~amplicon, LA1, method = "t.test", ref.group =".all.")
# write.csv(Ttest_FC, "Ttest_FeatureNorm_noLa1_20200731.csv")
# 
# write.csv(AvgAmp, "Overall_amplicon_means_20200731.csv")
# write.csv(Avg, "Overall_means_20200731.csv")
# 


graphG <- AnalysisLA1(stats_FC, "Average Normalized Reads per amplicoin")
pdf("Rplot_Optimisation20200720_NormalizedFC.pdf", width = 28, height = 20)
graphG[1]
graphG[2]
graphG[3]
graphG[4]
graphG[5]
graphG[6]
graphG[7]
graphG[8]
graphG[9]
graphG[10]
graphG[11]
graphG[12]
graphG[13]
graphG[14]
dev.off()

graphG <- AnalysisLA1(stats_Norm, "Average Normalized Coverage per amplicon")
pdf("Rplot_Optimisation20200720_NormalizadTotal.pdf", width = 28, height = 20)
graphG[1]
graphG[2]
graphG[3]
graphG[4]
graphG[5]
graphG[6]
graphG[7]
graphG[8]
graphG[9]
graphG[10]
graphG[11]
graphG[12]
graphG[13]
graphG[14]
dev.off()

g1 <- Stats_graph(stats_min1, "Minimal Coverage over 1 bp", "All samples, n=23")
g2 <- Stats_graph(stats_min50, "Minimal Coverage over 50 bp", "All samples, n=23")
g3 <- Stats_graph(stats_min100, "Minimal Coverage over 100 bp", "All samples, n=23")
g4 <- Stats_graph_no800(NMin1, "Normlaized Minimal Coverage over 1 bp", "All samples, n=23")
g5 <- Stats_graph_no800(NMin25, "Normlaized Minimal Coverage over 25 bp", "All samples, n=23")
g6 <- Stats_graph_no800(NMin50, "Normlaized Minimal Coverage over 50 bp", "All samples, n=23")

pdf("Rplot_MinimalCoverage",  width = 28, height = 20)
g1
g2
g3
g4
g5
g6
dev.off()

#means comparison 
LA1 <- subset(stats_avgCov, smpl %in% noLa1) %>% full_join(dfG1, by = "smpl")
Ttest_avgCov <- compare_means(stat~amplicon, stats_avgCov, method = "t.test", ref.group =".all.")
write.csv(Ttest_avgCov, "Ttest_avgCov.csv")
t <- compare_means(stat~amplicon, LA1, method = "t.test", ref.group =".all.")
write.csv(Ttest_avgCov, "Ttest_avgCov_noLA1.csv")
LA1 <- subset(stats_FC, smpl %in% noLa1) %>% full_join(dfG1, by = "smpl")
Ttest_FC <- compare_means(stat~amplicon, stats_FC, method = "t.test", ref.group =".all.")
write.csv(Ttest_FC, "Ttest_FeatureNorm.csv")
t <- compare_means(stat~amplicon, LA1, method = "t.test", ref.group =".all.")
write.csv(Ttest_FC, "Ttest_FeatureNorm_noLa1.csv")
