# """
# Aim: To collect (copy) all the nanopore *.sorted.bam.depth file in beluga to analyse the amplicon depth of each sample.
# Author: Anne-Marie Roy
# Date: Summer 2019
# """

#To be run from beluga in personnal scratch
cd /home/aroy4/scratch/
mkdir -p CovSeq/AmpTemp/

#Using otpio globs
shopt -s extglob

cd /genfs/projects/COVID_full_processing/nanopore/
for dir in */; do
  cd $dir/analysis/
  for dir2 in *nanopolish_800x/; do
    cd $dir2/
    for sample in !(multiqc_data)/; do
      cd $sample/
      file=${sample::-1}.sorted.bam.depth
      if [[ ! -e /home/aroy4/scratch/CovSeq/AmpTemp/$file ]]; then
        cp *.sorted.bam.depth /home/aroy4/scratch/CovSeq/AmpTemp/
      fi
      cp *.metrics.csv /home/aroy4/scratch/CovSeq/AmpTemp/
      cd ..
    done
  cd ..
  done
  cd /genfs/projects/COVID_full_processing/nanopore/
  # pwd
done
