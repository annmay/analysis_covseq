#Statistical Analysis and Plot for the amplicon Depth Analysis 
#Author : Anne-Marie Roy
#Date: Summer 2020 

##Adapt those variable to your environement, give absolute path
# baseFolder <- "C:/Users/pepel/Desktop/Raccourcis_Anne/" #home computer
# baseFolder <- "/Users/aroy/BioinformaticsWorks/" #work computer
baseFolder <- "/Users/aroy//Documents/" #work computer2
# baseFolder <- "/home/aroy4/scratch/" #Beluga


DataFile <- paste(baseFolder, "analysis_covseq/AmpDataFormating.r", sep="")


#Data 
source(DataFile)
# source(GroupFunction)

ProjectPath <- paste(baseFolder, "Covseq/", sep="")

# setwd(ProjectPath)

#La1
LA_primer <- c('nCoV-2019_5', 'nCoV-2019_17', 'nCoV-2019_23', 'nCoV-2019_26', 'nCoV-2019_66',
               'nCoV-2019_70', 'nCoV-2019_74', 'nCoV-2019_91', 'nCoV-2019_97', 'nCoV-2019_64')
#La2 
# LA_primer <- c("nCoV-2019_9_", "nCoV-2019_18_", "nCoV-2019_20_", "nCoV-2019_31_", "nCoV-2019_36_",
#                  "nCoV-2019_81_", "nCoV-2019_86_", "nCoV-2019_94_", "nCoV-2019_13_", "nCoV-2019_64_")


#Plots (overall)

#Basic Plots; Box Plot of every amplicon w/ overall median
listTitle <- names(list_df) #Change this one according to your desired titles

pdf(paste(ProjectPath, "testAmp_BasicGraph.pdf", sep=""), width = 28, height = 20)
for (i in 1:length(list_df)){
  titre = listTitle[i]
  stitre = paste("N = ", length(unique(list_df[[i]]$smpl)))
  g <- basicGraph(list_df[[i]], titre, stitre)
 
  print(g)
}
dev.off()

#StatsGraph; BasicGraph with anova test results and pValue between each amplicon on the overall mean
pdf(paste(ProjectPath, "Amp_StatsGraph.pdf", sep=""), width = 28, height = 20)
for (i in 1:length(list_df)){
  titre = listTitle[i]
  # print(paste(titre, max(df$stat)))
  stitre = paste("N = ", length(unique(list_df[[i]]$smpl)))
  df <- drop_na(list_df[[i]])
  g <- StatsGraph(df, titre, stitre, LA_primer)
  
  print(g)
}
dev.off()


#T-test (overall)
for (i in 1:length(list_df)){
  titre = paste(ProjectPath, "AmpAnalysis/T_test/Ttest_global_", listTitle[i], ".csv", sep="")
  t <- compare_means(stat~amplicon, list_df[[i]], method = "t.test", ref.group =".all.")
  write.csv(t, titre)
}


##Grouped analysis 

#Unpaired
if (length(list_group_Unpaired)>0){
  pdf(paste(ProjectPath, "OverallStatsUnpaired.pdf", sep=""), width = 36, height = 20)
  for (i in 1:length(list_group_Unpaired)){
    df <- list_group_Unpaired[[i]]
    n <- length(unique(df$smpl))
    titre <- names(list_group_Unpaired[i])
    stitre <- paste("n= ", n, sep="")
    
    g <- StatsGraph(df, titre, stitre, LA_primer) 
    print(g)
    
    g2 <- g + facet_grid(group~.)
    print(g2)
    
    for (k in unique(list_df_unpaired[[i]]$group)){
      df <- list_group_Unpaired[[i]] %>% filter(group == k)
      n <- length(unique(df$smpl))
      titre <- names(list_group_Unpaired[i])
      # print(paste(titre, max(df$stat)))
      stitre <- paste("group: ", k, ", n= ", n, sep="")
      g <- StatsGraph(df, titre, stitre, LA_primer)

      print(g)
    }
  }
  dev.off()
  print("unpaired-1 Finish")
  pdf(paste(ProjectPath, "UnpairedGroupAnlaysis.pdf", sep=""), width = 28, height = 20)
  for (i in 1:length(list_group_Unpaired)){
    g1=filter(list_group_Unpaired[[i]], group==unique(list_group_Unpaired[[i]]$group)[1])
    g2=filter(list_group_Unpaired[[i]], group==unique(list_group_Unpaired[[i]]$group)[2])
              
    # print(paste(unique(list_group_Unpaired[[i]]$group)[1], "len = ", length(unique(g1$smpl)),
    #             unique(list_group_Unpaired[[i]]$group)[2], "len = ", length(unique(g2$smpl))))
    # print(length(unique(list_group_Unpaired[[i]]$group)))
    titre = names(list_group_Unpaired)[i] ##FC normalisation
    stitre = paste(listTitle[2], ", n= ", length(unique(list_group_Unpaired[[i]]$smpl))/length(unique(list_group_Unpaired[[i]]$group)))
    
    g1 <- StatsGraphGroup(list_group_Unpaired[[i]], titre, stitre, LA_primer)
    g2 <- GroupPlotUnpaired(list_group_Unpaired[[i]], titre, stitre, LA_primer)

    print(g1)
    print(g2)
  }
  dev.off()
}



###Plots


###T-test
if (length(list_group_Unpaired)>0){
  for (i in 1:length(list_group_Unpaired)){
    titre = paste(ProjectPath, "AmpAnalysis/T_test/Ttest_groupUnpaired_global_", names(list_group_Unpaired)[i], ".csv", sep="")
    try(t <- compare_means(stat~group, list_group_Unpaired[[i]], method = "t.test", 
                           paired = FALSE, group.by = "amplicon"))
    write.csv(t, titre)
  }
  for (i in 1:length(list_group_Unpaired)){
    for (k in unique(list_df_unpaired[[i]]$group)){
      df <- list_group_Unpaired[[i]] %>% filter(group == k)
      titre = paste(ProjectPath, "AmpAnalysis/T_test/Ttest_intragroupUnpaired_", names(list_group_Unpaired)[i], "_", k,  ".csv", sep="")
      t <- try(compare_means(stat~smpl, df, method = "t.test", 
                             ref.group = ".all."))
      write.csv(t, titre)
    }
  }
}

##CSV with the average of each group per amplicon 
list_averages <- list()
for (i in 1:length(list_group_Unpaired)){
  avg <- list_group_Unpaired[[i]] %>% group_by(amplicon, group) %>% dplyr::summarise(Mean=mean(stat))
  avg <- avg %>% spread(group, Mean)
  titre = gsub(" ", "_", names(list_group_Unpaired)[i], fixed=TRUE)
  filename = paste(ProjectPath, "AmpAnalysis/T_test/Average_FC", titre, "_", ".csv", sep="")
  write.csv(avg, filename)
  list_averages[[i]] <- avg 
}

list_averages <- list()
for (i in 1:length(list_group_Unpaired_avg)){
  avg <- list_group_Unpaired_avg[[i]] %>% group_by(amplicon, group) %>% dplyr::summarise(Mean=mean(stat))
  avg <- avg %>% spread(group, Mean)
  titre = gsub(" ", "_", names(list_group_Unpaired_avg)[i], fixed=TRUE)
  filename = paste(ProjectPath, "AmpAnalysis/T_test/Average_", titre, "_", ".csv", sep="")
  write.csv(avg, filename)
  list_averages[[i]] <- avg 
}

###Plots 


###T-test

#Paired
if (length(list_group_Paired)>0){
  pdf(paste(ProjectPath, "OverallStatsPaired.pdf", sep=""), width = 28, height = 20)
  for (i in 1:length(list_group_Paired)){
      for (k in unique(list_df_paired[[i]]$group)){
        df <- list_group_Paired[[i]] %>% filter(group == k)
        n <- length(unique(df$smpl))
        titre <- names(list_group_Paired[i])
        stitre <- paste("group: ", k, ", n= ", n, sep="")
        g <- StatsGraph(df, titre, stitre, LA_primer)
        
        print(g)
      }
  }
  dev.off()
  
  pdf(paste(ProjectPath, "PairedGroupAnlaysis.pdf", sep=""), width = 28, height = 20)
  for (i in 1:length(list_group_Paired)){
    titre = names(list_group_Paired)[i] 
    stitre = paste(listTitle[2], ", n= ", length(unique(list_group_Paired[[i]]$smpl))) ##FC normalisation

    g1 <- StatsGraphGroup(list_group_Paired[[i]], titre, stitre, LA_primer)
    g2 <- GroupPlotPaired(list_group_Paired[[i]], titre, stitre, LA_primer)
    g3 <- PairedPlot(list_group_Paired[[i]], titre, stitre)

    print(g1)
    print(g2)
    print(g3)
  }
  dev.off()
}



###Plots


###T-test
if (length(list_group_Paired)>0){
  for (i in 1:length(list_group_Paired)){
    titre = paste(ProjectPath, "AmpAnalysis/T_test/Ttest_groupPaired_global_", names(list_group_Paired)[i], ".csv", sep="")
    try(t <- compare_means(stat~group, list_group_Paired[[i]], method = "t.test", 
                       paired = TRUE, group.by = "amplicon"))
    g1=filter(list_group_Paired[[i]], group==unique(list_group_Paired[[i]]$group)[1])
    g2=filter(list_group_Paired[[i]], group==unique(list_group_Paired[[i]]$group)[2])
    print(paste(unique(list_group_Paired[[i]]$group)[1], "len = ", length(unique(g1$smpl)),
                            unique(list_group_Paired[[i]]$group)[2], "len = ", length(unique(g2$smpl))))
    
    if(class(t)[1] != "try-error"){
      write.csv(t, titre)
    }
  }
  for (i in 1:length(list_group_Paired)){
    for (k in unique(list_df_paired[[i]]$group)){
      df <- list_group_Paired[[i]] %>% filter(group == k)
      titre = sub(" ", "_", names(list_group_Paired)[i])
      filename = paste(ProjectPath, "AmpAnalysis/T_test/Ttest_intragroupPaired_", titre, "_", k,  ".csv", sep="")
      t <- try(compare_means(stat~smpl, df, method = "t.test", 
                         ref.group = ".all."))
      class_t <- class(t)
      if(class(t)[1] != "try-error"){
        write.csv(t, filename)
      }
    }
  }
}

#Variance of Artic and NEB primers 
artic <- g1
NEB <- g2

var(artic$stat)
#2766810
var(NEB$stat)
#3163444


sd(artic$stat)
#1663.373
sd(NEB$stat)
#1778.607


#CV 
sd(artic$stat, na.rm=TRUE)/
  mean(artic$stat, na.rm=TRUE)*100
#50.00967%
sd(NEB$stat, na.rm=TRUE)/
  mean(NEB$stat, na.rm=TRUE)*100
#61.16193%



global <- list_averages[[1]]

sd(global$Artic, na.rm=TRUE)/
  mean(global$Artic, na.rm=TRUE)*100
#39.79633%
sd(global$NEB, na.rm=TRUE)/
  mean(global$NEB, na.rm=TRUE)*100
#31.03161%


###Coeff de var 
##Format des donn�es --> artic et NEB prob OK 
##Cr�ation d'un df vide
df_CV <- setNames(data.frame(matrix(ncol = 5, nrow = 0)), c("amplicon", "CV_artic_primers", "CV_NEB_primers", 
                                                            "mean_artic", "mean_NEB")) 
df_CV$amplicon <- as.character(df_CV$amplicon)
df_CV <- df_CV %>% add_row(amplicon="global", 
               CV_artic_primers=(sd(global$Artic, na.rm=TRUE)/
                           mean(global$Artic, na.rm=TRUE)*100), 
               CV_NEB_primers=(sd(global$NEB, na.rm=TRUE)/
                           mean(global$NEB, na.rm=TRUE)*100),
               mean_artic=mean(global$Artic, na.rm=TRUE),
               mean_NEB=mean(global$NEB, na.rm=TRUE))
amplicons <- as.character(unique(artic$amplicon))
for (amp in amplicons){
  #amp <- amplicons[[1]]
  articAmp <- artic %>% filter(amplicon == amp)
  nebAmp <- NEB %>% filter(amplicon == amp)
  df_CV <- df_CV %>% add_row(amplicon=amp, 
                    CV_artic_primers=(sd(articAmp$stat, na.rm=TRUE)/
                                        mean(articAmp$stat, na.rm=TRUE)*100), 
                    CV_NEB_primers=(sd(nebAmp$stat, na.rm=TRUE)/
                                      mean(nebAmp$stat, na.rm=TRUE)*100),
                    mean_artic=mean(articAmp$stat, na.rm=TRUE),
                    mean_NEB=mean(nebAmp$stat, na.rm=TRUE))
}
write.csv(df_CV, "Statistics_averageDepth_NEBprimers.csv")

###Using the FC 
df_norm<- list_group_Unpaired[[1]]
artic <- filter(df_norm, group == "Artic")
NEB <- filter(df_norm, group == "NEB")
df_CV_norm <- setNames(data.frame(matrix(ncol = 5, nrow = 0)), c("amplicon", "CV_artic_primers", "CV_NEB_primers", 
                                                            "mean_artic", "mean_NEB")) 
df_CV_norm$amplicon <- as.character(df_CV_norm$amplicon)
df_CV_norm <- df_CV_norm %>% add_row(amplicon="global", 
                           CV_artic_primers=(sd(artic$stat, na.rm=TRUE)/
                                               mean(artic$stat, na.rm=TRUE)*100), 
                           CV_NEB_primers=(sd(artic$stat, na.rm=TRUE)/
                                             mean(NEB$stat, na.rm=TRUE)*100),
                           mean_artic=mean(artic$stat, na.rm=TRUE),
                           mean_NEB=mean(NEB$stat, na.rm=TRUE))
amplicons <- as.character(unique(artic$amplicon))
for (amp in amplicons){
  #amp <- amplicons[[1]]
  articAmp <- artic %>% filter(amplicon == amp)
  nebAmp <- NEB %>% filter(amplicon == amp)
  df_CV_norm <- df_CV_norm %>% add_row(amplicon=amp, 
                             CV_artic_primers=(sd(articAmp$stat, na.rm=TRUE)/
                                                 mean(articAmp$stat, na.rm=TRUE)*100), 
                             CV_NEB_primers=(sd(nebAmp$stat, na.rm=TRUE)/
                                               mean(nebAmp$stat, na.rm=TRUE)*100),
                             mean_artic=mean(articAmp$stat, na.rm=TRUE),
                             mean_NEB=mean(nebAmp$stat, na.rm=TRUE))
}
write.csv(df_CV_norm, "Statistics_NormFeatureCount_NEBprimers.csv")

