#!/bin/bash
#SBATCH --account=def-ioannisr
#SBATCH --time=02:00:00
#SBATCH --job-name=AmpliconAnalysis
#SBATCH --output=/home/aroy4/scratch/CovSeq/R_analysis.out

module load python/3.8.2
source amp-env/bin/activate
module load samtools/1.10
module load gcc/7.3.0 r/4.0.2

Rscript /home/aroy4/scratch/analysis_covseq/AmpAnalysis.r
