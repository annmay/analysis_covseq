# """
# Aim: To collect (copy) all the nanopore *.sorted.bam.depth file in beluga to analyse the amplicon depth of each sample.
# Author: Anne-Marie Roy
# Date: Summer 2019
# """

#To be run from beluga in personnal scratch
cd /home/aroy4/scratch/
mkdir -p SampleDepthFile/Tests2

cd /genfs/projects/COVID_full_processing/nanopore/LSPQ_Outbreak902_PAE49634_2020072/analysis/LSPQoutbreak902_noBC_nanopolish_800x
for dir in M*/; do
  cd $dir/
  # pwd
  cp *.sorted.bam.depth /home/aroy4/scratch/SampleDepthFile/Tests2
  cp *.clean.sorted.bam /home/aroy4/scratch/SampleDepthFile/Tests2
  cd ..
  done
# cd /lustre04/scratch/hgalvez/Behr_and_LowTempTest/analysis/Behr_LowTemp_nanopolish_800x


cd /home/aroy4/scratch/SampleDepthFile/
