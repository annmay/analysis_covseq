##GRaph LA2 -Vizualisation 


#Path for the file w/ functions (AmpliconStats.r)
pathfun = "C:/Users/pepel/Desktop/Raccourcis_Anne/analyse_covseq/AnalysisLA1.r"
# pathfun = "/Users/aroy/BioinformaticsWorks/analysis_covseq/AmpliconStats.r"

#Library 
source(pathfun)
library(ggpubr)


#Subset noLA1 and plot a bar graph of the minimum with bar at LA2 position. 
min_noLA1 <- subset(stats_FC, smpl %in% noLa1) %>% group_by(amplicon) %>% summarise_at(vars(stat), list(min = min))
LA1_primer <- c('nCoV-2019_5_', 'nCoV-2019_17_', 'nCoV-2019_23_', 'nCoV-2019_26_', 'nCoV-2019_66_',
                'nCoV-2019_70_', 'nCoV-2019_74_','nCoV-2019_91_', 'nCoV-2019_97_', 'nCoV-2019_64_')

LA2_primer1 <- c("nCoV-2019_9_", "nCoV-2019_18_", "nCoV-2019_20_", "nCoV-2019_31_", "nCoV-2019_36_",
                 "nCoV-2019_81_", "nCoV-2019_86_", "nCoV-2019_94_", "nCoV-2019_13_", "nCoV-2019_64_")

LA2_primer2 <- c("nCoV-2019_94_"," nCoV-2019_26_", "nCoV-2019_64_", "nCoV-2019_81_"," nCoV-2019_91_",
                "nCoV-2019_36_", "nCoV-2019_31_", "nCoV-2019_17_", "nCoV-2019_70_", "nCoV-2019_9_",
                "nCoV-2019_18_", "nCoV-2019_74_")

lineV.data <- data.frame(xintercept = LA1_primer)
colors <- ifelse(levels(stats_FC$amplicon) %in% LA1_primer, "red", "black")
line.data <- data.frame(yintercept = c(median(stats_FC$stat)), Lines = c("Median"))
La2Col <- ifelse(levels(stats_FC$amplicon) %in% LA2_primer1, "lightseagreen", "grey")


g1 <- ggplot(min_noLA1, aes(amplicon, min)) + geom_bar(stat = "identity", fill = La2Col) +
  geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1) +
  geom_vline(aes(xintercept = xintercept), lineV.data, lwd=0.4, color = "red") +
  labs(title="LA2 Pool - Option 1", 
       subtitle = "Normalized coverage w/o LA1") +
  theme(legend.position="bottom", 
        plot.title = element_text(hjust = 0.5), 
        plot.subtitle = element_text(hjust = 0.5),
        axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2, colour = colors))

La2Col2 <- ifelse(levels(stats_FC$amplicon) %in% LA2_primer2, "lightpink", "grey")
g2 <- ggplot(min_noLA1, aes(amplicon, min)) + geom_bar(stat = "identity", fill = La2Col2) +
  geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1) +
  geom_vline(aes(xintercept = xintercept), lineV.data, lwd=0.4, color = "red") +
  labs(title="LA2 Pool - Option 2", 
       subtitle = "Normalized coverage w/o LA1") +
  theme(legend.position="bottom", 
        plot.title = element_text(hjust = 0.5), 
        plot.subtitle = element_text(hjust = 0.5),
        axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2, colour = colors))
pdf("LA2_options.pdf", width = 28, height = 20)
g1
g2
dev.off()
