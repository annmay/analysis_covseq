# Program to visualized  the amplicon coverage from CovSeq 
# Anne-Marie Roy
# Summer 2020

#Define path for the stats files 
# path = "/Users/aroy/BioinformaticsWorks/CovidNano_Stats"
path = "/Users/aroy/BioinformaticsWorks/ArticNano_TestStats"


#Library 
library(tidyverse)
library(plyr)
library(RColorBrewer)
library(pals)
library(reshape2)
library(ggpubr)

#Data 
#make a function to extract all data from the folder and import it. put the file name in a list 
files <- list.files(path = path)
list_df <- list()
  
  
# Functions
compiled_df <- function(filecsv, n) {
  temp <- read.csv(filecsv)[1] %>% bind_cols(read.csv(filecsv)[n])
  nom <- gsub( "Depth|_*statistics.csv", "", filecsv)
  colnames(temp) <- c("amplicon", nom)
  list_df <- append(list_df, temp)
  assign(nom, temp, env=.GlobalEnv)
  return(list_df)
}

Merging_Stats <- function(filescsv, n){
  
  dfs <- lapply(filescsv,
                 function(f){
                   list_df <- compiled_df(f, n)
                 })
  
  stats <- Reduce(function(x,y) merge(x,y,by="amplicon",all=TRUE), dfs)
  
  stats <- stats %>% gather(key = "smpl", value = stat, -amplicon) 
  stats$amplicon <- factor(stats$amplicon, levels = unique(stats_df$amplicon))
  return(stats)
}

Stats_graph <- function(df, Title, subtitle) {
  colourCount = length(unique(df$amplicon))
  line.data <- data.frame(yintercept = c(median(df$stat), 800), Lines = c("Median", "800X Coverage"))
  LA1_primer <- c('nCoV-2019_5_', 'nCoV-2019_17_', 'nCoV-2019_23_', 'nCoV-2019_26_', 'nCoV-2019_66_',
                  'nCoV-2019_70_', 'nCoV-2019_74_', 'nCoV-2019_91_', 'nCoV-2019_97_', 'nCoV-2019_64_')
  lineV.data <- data.frame(xintercept = LA1_primer)
  colors <- ifelse(levels(df$amplicon) %in% LA1_primer, "red", "black")
  
  g <- ggplot(df, aes(amplicon, stat)) + 
    geom_boxplot(aes(fill=amplicon)) +
    scale_fill_manual(values = rep(as.vector(stepped(24)),5)[1:colourCount]) +
    geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1)+
    # geom_vline(aes(xintercept = xintercept), lineV.data, lwd=0.7)+
    stat_compare_means(method = "anova", label.x = 4, label.y = 0-max(df$stat)*0.008) +
    stat_compare_means(method = "t.test", ref.group = ".all.", hide.ns = TRUE, 
                       label = "p.signif", position = position_jitter(width = 0, height = max(df$stat)*0.01, seed = 4)) +
    labs(title=Title, 
         subtitle = subtitle) +
    theme(legend.position="bottom", 
          plot.title = element_text(hjust = 0.5), 
          plot.subtitle = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2, colour = colors)) +
    guides(fill=guide_legend(nrow=10)) 
  
  return(g)
}

Stats_graph_no800 <- function(df, Title, subtitle) {
  colourCount = length(unique(df$amplicon))
  line.data <- data.frame(yintercept = c(median(df$stat)), Lines = c("Median"))
  LA1_primer <- c('nCoV-2019_5_', 'nCoV-2019_17_', 'nCoV-2019_23_', 'nCoV-2019_26_', 'nCoV-2019_66_',
                  'nCoV-2019_70_', 'nCoV-2019_74_', 'nCoV-2019_91_', 'nCoV-2019_97_', 'nCoV-2019_64_')
  lineV.data <- data.frame(xintercept = LA1_primer)
  colors <- ifelse(levels(df$amplicon) %in% LA1_primer, "red", "black")
  heigthP <- ifelse(max(df$stat)*0.01<0, max(df$stat)*0.01, 0.0005)

  g <- ggplot(df, aes(amplicon, stat)) + 
    geom_boxplot(aes(fill=amplicon)) +
    scale_fill_manual(values = rep(as.vector(stepped(24)),5)[1:colourCount]) +
    geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1)+
    #geom_vline(aes(xintercept = xintercept), lineV.data, lwd=0.7)+
    stat_compare_means(method = "anova", label.x = 4, label.y = 0-max(df$stat)*0.008) +
    # stat_compare_means(method = "t.test", ref.group = ".all.", hide.ns = TRUE, 
    #                    label = "p.signif", position = position_jitter(width = 0, height = heigthP, seed = 4)) +
    labs(title=Title, 
         subtitle = subtitle) +
    theme(legend.position="bottom", 
          plot.title = element_text(hjust = 0.5), 
          plot.subtitle = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2, colour = colors)) +
    guides(fill=guide_legend(nrow=10)) 
  
  return(g)
}

Stats_graph_Compare <- function(df, Title, subtitle) {
  line.data <- data.frame(yintercept = c(median(df$stat)), Lines = c("Median"))
  LA1_primer <- c('nCoV-2019_5_', 'nCoV-2019_17_', 'nCoV-2019_23_', 'nCoV-2019_26_', 'nCoV-2019_66_',
                  'nCoV-2019_70_', 'nCoV-2019_74_', 'nCoV-2019_91_', 'nCoV-2019_97_', 'nCoV-2019_64_')
  lineV.data <- data.frame(xintercept = LA1_primer)
  
  g <-ggplot(df, aes(amplicon, stat)) + 
    geom_boxplot(aes(fill=group)) +
    geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1)+
    geom_vline(aes(xintercept = xintercept), lineV.data, lwd=0.7)+
    labs(title=Title, 
         subtitle = subtitle) +
    theme(legend.position="bottom", 
          plot.title = element_text(hjust = 0.5), 
          plot.subtitle = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2))
  
  return(g)
}

Stats_graph_CompareFacet <- function(df, Title, subtitle) {
  line.data <- data.frame(yintercept = c(median(df$stat)), Lines = c("Median"))
  LA1_primer <- c('nCoV-2019_5_', 'nCoV-2019_17_', 'nCoV-2019_23_', 'nCoV-2019_26_', 'nCoV-2019_66_',
                  'nCoV-2019_70_', 'nCoV-2019_74_', 'nCoV-2019_91_', 'nCoV-2019_97_', 'nCoV-2019_64_')
  lineV.data <- data.frame(xintercept = LA1_primer)
  
  g <-ggplot(df, aes(group, stat)) + 
    geom_boxplot(aes(fill=group)) +
    geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1)+
    geom_vline(aes(xintercept = xintercept), lineV.data, lwd=0.7)+
    facet_wrap(~amplicon,  scales = "free_y") +
    stat_compare_means(label = "p.format", method = "t.test", label.x = 0.7, label.y =0) +
    geom_rect(data=subset(df, amplicon %in% LA1_primer), 
              aes(xmin=-Inf, xmax=Inf, ymin=-Inf, ymax=Inf), 
              fill="lightseagreen", alpha=0.05)+
    labs(title=Title, 
         subtitle = subtitle) +
    theme(legend.position="left", 
          plot.title = element_text(hjust = 0.5), 
          plot.subtitle = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2))
  
  return(g)
}

Stats_graph_CompareLA1Facet <- function(df, Title, subtitle) {
  line.data <- data.frame(yintercept = c(median(df$stat)), Lines = c("Median"))
  LA1_primer <- c('nCoV-2019_5_', 'nCoV-2019_17_', 'nCoV-2019_23_', 'nCoV-2019_26_', 'nCoV-2019_66_',
                  'nCoV-2019_70_', 'nCoV-2019_74_', 'nCoV-2019_91_', 'nCoV-2019_97_', 'nCoV-2019_64_')
  lineV.data <- data.frame(xintercept = LA1_primer)
  
  df <- df %>% filter(amplicon %in% LA1_primer)
  
  g <-ggplot(df, aes(group, stat)) + 
    geom_boxplot(aes(fill=group)) +
    geom_hline(aes(yintercept = yintercept, linetype = Lines, color = Lines), line.data, lwd = 1)+
    geom_vline(aes(xintercept = xintercept), lineV.data, lwd=0.7)+
    facet_wrap(~amplicon,  scales = "free_y") +
    stat_compare_means(label = "p.format", method = "t.test", label.x = 0.7, label.y = 0) +
    labs(title=Title, 
         subtitle = subtitle) +
    theme(legend.position="left", 
          plot.title = element_text(hjust = 0.5), 
          plot.subtitle = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 65, vjust = 1.2, hjust=1.2))
  
  return(g)
}

# data wrangling 
# setwd(path)
# stats_df <- read.csv(files[1])[1]
# colnames(stats_df) <- c("amplicon")
# ord <- factor(stats_df$amplicon, levels = unique(stats_df$amplicon))
# 
# stats_avgCov <- Merging_Stats(files, 2, "AvgCov")
# stats_abnt1 <- Merging_Stats(files, 3, "Normalized abundance")
# stats_NormAvg <- Merging_Stats(files, 4, "Normalized AvgCov")
# stats_FC <- Merging_Stats(files, 5, "NormFC AvgCov")
# stats_min1 <- Merging_Stats(files, 6, "MinCov_1bp")
# stats_min50 <- Merging_Stats(files, 7, "MinCov_50bp")
# stats_min100 <- Merging_Stats(files, 8, "MinCov_100bp")
# 
# setwd("../")
# 
# #Select only sample treated without LA1 primer pool
# noLA1 <- c('L00240581', 'L00240804', 'L00240811', 'L00240842', "L00241349", 'L00241654', 
#            'L00241847', 'L00242047', 'L00242078', 'L00242082', 'L00242229', "L00242236")
# 
# stats_avg_noLA1 <- subset(stats_avgCov, smpl %in% noLA1)
# stats_abnt1_noLA1 <- subset(stats_abnt1, smpl %in% noLA1)
# stats_NormAvg_noLA1 <- subset(stats_NormAvg, smpl %in% noLA1)
# stats_FC_noLA1 <- subset(stats_FC, smpl %in% noLA1)
# stats_min1_noLA1 <- subset(stats_min1, smpl %in% noLA1)
# stats_min50_noLA1 <- subset(stats_min50, smpl %in% noLA1)
# stats_min100_noLA1 <- subset(stats_min100, smpl %in% noLA1)
# 
# #Select only samples treated with LA1 pool 
# stats_avg_wLA1 <- subset(stats_avgCov, !(smpl %in% noLA1))
# stats_abnt1_wLA1 <- subset(stats_abnt1, !(smpl %in% noLA1))
# stats_min1_wLA1 <- subset(stats_min1, !(smpl %in% noLA1))
# stats_min50_wLA1 <- subset(stats_min50, !(smpl %in% noLA1))
# stats_min100_wLA1 <- subset(stats_min100, !(smpl %in% noLA1))
# 
# #Add LA1 information to the df with all the data 
# stats_avgCov$poolLA1 <- ifelse(stats_avgCov$smpl %in% noLA1, FALSE, TRUE) 
# stats_abnt1$poolLA1 <- ifelse(stats_abnt1$smpl %in% noLA1, FALSE, TRUE) 
# stats_NormAvg$poolLA1 <-  ifelse(stats_NormAvg$smpl %in% noLA1, FALSE, TRUE) 
# stats_FC$poolLA1 <-  ifelse(stats_FC$smpl %in% noLA1, FALSE, TRUE) 
# 
# 
# #Make a boxplots of the data 
# g_avgCov <- Stats_graph(stats_avgCov, "Average Coverage of each amplicon", "Nanopore Data, n=496")
# g_abnt1 <- Stats_graph_no800(stats_abnt1, "Normalized Coverage of each amplicon", "Nanopore Data, n=496, Normalized on total reads")
# g_Normavg <- Stats_graph_no800(stats_NormAvg, "Normalized Average Coverage of each amplicon", "Nanopore Data, n=496")
# g_FC <- Stats_graph_no800(stats_FC, "Average Coverage of each amplicon with Feature Normalization", "Nanopore Data, n=496")
# g_min1 <- Stats_graph(stats_min1, "Minimum Coverage over 1pb for each amplicon", "Nanopore Data, n=496")
# g_min50 <- Stats_graph(stats_min50, "Minimum Coverage over 50pb for each amplicon", "Nanopore Data, n=496")
# g_min100 <- Stats_graph(stats_min100, "Minimum Coverage over 100pb for each amplicon", "Nanopore Data, n=496")
# 
# 
# pdf("Rplot_amplicon_Cov_NanoporeTest.pdf", width = 20, height = 12)
# g_avgCov
# #g_Normavg
# g_FC
# #g_abnt1
# g_min1
# g_min50
# g_min100
# dev.off()
# 
# 
# g_avgCov <- Stats_graph(stats_avg_noLA1, "Average Coverage of each amplicon", "Nanopore Data w/o LA1, n=12")
# g_abnt1 <- Stats_graph_no800(stats_abnt1_noLA1, "Normalized Coverage of each amplicon", "Nanopore Data w/o LA1, n=12, Normalized on total reads")
# g_Normavg <- Stats_graph_no800(stats_NormAvg_noLA1, "Normalized Average Coverage of each amplicon", "Nanopore Data w/o LA1, n=12")
# g_FC <- Stats_graph_no800(stats_FC_noLA1, "Average Coverage of each amplicon with Feature Normalization", "Nanopore Data w/o LA1, n=12")
# g_min1 <- Stats_graph(stats_min1_noLA1, "Minimum Coverage over 1pb for each amplicon", "Nanopore Data w/o LA1, n=12")
# g_min50 <- Stats_graph(stats_min50_noLA1, "Minimum Coverage over 50pb for each amplicon", "Nanopore Data w/o LA1, n=12") 
# g_min100 <- Stats_graph(stats_min100_noLA1, "Minimum Coverage over 100pb for each amplicon", "Nanopore Data w/o LA1, n=12") 
# 
# 
# pdf("Rplot_amplicon_Cov_Nanopore_noLA1Test.pdf", width = 20, height = 12)
# g_avgCov
# #g_Normavg
# g_FC
# #g_abnt1 
# g_min1
# g_min50
# g_min100
# dev.off()
# 
# 
# # Calcul anova pour trouver les amplicon significativement plus haut/bas que les autres. 
# #utilise stats_avgCov group? par amplicon 
# #Observe amplicon distribution 
# g <- ggplot(stats_avgCov, aes(stat)) + 
#   geom_density() +
#   facet_wrap(stats_avgCov$amplicon) +
#   labs(title="Average Coverage Distribution per amplicon", 
#        subtitle = "Nanopore Data, n=496")
# 
# g2 <- ggplot(stats_abnt1, aes(stat)) + 
#   geom_density() +
#   facet_wrap(stats_avgCov$amplicon) +
#   labs(title="Normalized total number of reads per amplicon Distribution", 
#        subtitle = "Nanopore Data, n=496")
# 
# g3 <- ggplot(stats_NormAvg, aes(stat)) + 
#   geom_density() +
#   facet_wrap(stats_avgCov$amplicon) +
#   labs(title="Distribution of average coverage with feature normalization", 
#        subtitle = "Nanopore Data, n=496")
# 
# g4 <- ggplot(stats_FC, aes(stat)) + 
#   geom_density() +
#   facet_wrap(stats_avgCov$amplicon) +
#   labs(title="Feature Normalized Average Coverage per amplicon Distribution", 
#        subtitle = "Nanopore Data, n=496")
# 
# 
# pdf("Rplot_ampliconAvgCov_Distribution_NanoporeTest.pdf", width = 20, height = 12)
# g
# # g2
# # g3
# g4
# dev.off()

#Conclusion I need a better normalization 
#I probably need to adress that in the Python script... 

