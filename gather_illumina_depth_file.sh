# """
# Aim: To collect (copy) all the nanopore *.sorted.bam.depth file in beluga to analyse the amplicon depth of each sample.
# Author: Anne-Marie Roy
# Date: Summer 2019
# """

#To be run from beluga in personnal scratch
cd /home/aroy4/scratch/
mkdir -p CovSeq/AmpTemp/

#Using otpio globs
shopt -s extglob

i=1
cd /genfs/projects/COVID_full_processing/illumina/
for dir in */; do
  cd $dir/alignment/
  # pwd
  for dir2 in */; do
    cd $dir2
    pwd
    sample=${dir2::-1}
    if [[ ! -e /home/aroy4/scratch/CovSeq/AmpTemp/$sample.sorted.filtered.bam.depth ]]; then
      samtools depth $sample.sorted.filtered.bam > /home/aroy4/scratch/CovSeq/AmpTemp/$sample.sorted.filtered.bam.depth
      cd ../
    fi
    # pwd
  done
  cd /genfs/projects/COVID_full_processing/illumina/$dir/metrics
  cp metrics.csv /home/aroy4/scratch/CovSeq/AmpTemp/S$i.metrics.csv
  i=$((i+1))
  cd /genfs/projects/COVID_full_processing/illumina/
  # pwd
done

cd /lustre04/scratch/hgalvez/200619_M03555_0529_000000000-J6T6K/alignment/
# for dir in */; do
  for dir2 in */; do
    cd $dir2
    # pwd
    sample=${dir2::-1}
    if [[ ! -e /home/aroy4/scratch/CovSeq/AmpTemp/$sample.sorted.filtered.bam.depth ]]; then
      samtools depth $sample.sorted.filtered.bam > /home/aroy4/scratch/CovSeq/AmpTemp/$sample.sorted.filtered.bam.depth
      cd ../
    fi
    # pwd
  done
  cd /lustre04/scratch/hgalvez/200619_M03555_0529_000000000-J6T6K/metrics
  cp metrics.csv /home/aroy4/scratch/CovSeq/AmpTemp/S$i.metrics.csv
  i=$((i+1))
  cd /lustre04/scratch/hgalvez/200619_M03555_0529_000000000-J6T6K
  # pwd
# done


cd /home/hgalvez/scratch/20200828_LSPQ_GQ0011-0018_CTL/alignment/
# for dir in */; do
  for dir2 in */; do
    cd $dir2
    # pwd
    sample=${dir2::-1}
    if [[ ! -e /home/aroy4/scratch/CovSeq/AmpTemp/$sample.sorted.filtered.bam.depth ]]; then
      samtools depth $sample.sorted.filtered.bam > /home/aroy4/scratch/CovSeq/AmpTemp/$sample.sorted.filtered.bam.depth
      cd ../
    fi
    # pwd
  done
  cd /home/hgalvez/scratch/20200828_LSPQ_GQ0011-0018_CTL/metrics
  cp metrics.csv /home/aroy4/scratch/CovSeq/AmpTemp/S$i.metrics.csv
  i=$((i+1))
  cd /home/hgalvez/scratch/20200828_LSPQ_GQ0011-0018_CTL
  # pwd
# done
