# """
# Aim: To collect (copy) all the nanopore *.sorted.bam.depth file in beluga to analyse the amplicon depth of each sample.
# Author: Anne-Marie Roy
# Date: Summer 2019
# """

#To be run from beluga in personnal scratch
cd /home/aroy4/scratch/
mkdir -p CovSeq/AmpTemp/

#Using otpio globs
shopt -s extglob

#Selecting specific Samples
samples=("L00326726001" "L00326737001" "L00326754001" "L00326765001" "L00326787001" "L00326798001" "PosCtrl_LSPQ_LSPQ928A")

cd /genfs/projects/COVID_full_processing/nanopore/LSPQ_Outbreak928A_PAG31852_20210305/analysis//LSPQoutbreak928A_noBC_nanopolish_800x/

for sample in ${samples[@]}; do
  cd $sample/
  file=${sample}.sorted.bam.depth
  if [[ ! -e /home/aroy4/scratch/AmpTemp/$file ]]; then
    cp *.sorted.bam.depth /home/aroy4/scratch/AmpTemp/
  fi
  cp *.metrics.csv /home/aroy4/scratch/AmpTemp/
  cd ..
done
cd /genfs/projects/COVID_full_processing/nanopore/
# pwd
